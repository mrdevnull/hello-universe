package main

import (
    "crypto/md5"
    "fmt"
)

func main() {
    fmt.Println("hello universe!")

    const hardCodedPassword = "password"
    fmt.Println(hardCodedPassword)

    passwordBad := "password"
    fmt.Println(passwordBad)

    passwordBetter := getPassword()
    fmt.Println(passwordBetter)

    data := []byte("string to hash")
    fmt.Printf("%x\n", md5.Sum(data))
}

func getPassword() string {
    return "password"
}
